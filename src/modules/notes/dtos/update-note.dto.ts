import { PickType } from '@nestjs/swagger';
import { NoteValidation } from '~modules/notes/validation/note.validation';

export class UpdateNoteDto extends PickType(
  NoteValidation, ['text'] as const
) {}
