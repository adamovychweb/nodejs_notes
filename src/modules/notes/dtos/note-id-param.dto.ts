import { IsMongoId, IsNotEmpty } from 'class-validator';

export class NoteIdParamDto {
  @IsMongoId()
  @IsNotEmpty()
  id: string;
}
