import { IsNumber, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetAllQueryDto {
  @Transform((offset) => Number(offset.value))
  @IsNumber()
  @IsOptional()
  readonly offset = 0;

  @Transform((limit) => Number(limit.value))
  @IsNumber()
  @IsOptional()
  readonly limit = 0;
}
