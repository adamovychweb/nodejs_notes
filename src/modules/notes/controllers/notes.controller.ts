import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards
} from '@nestjs/common';

import { NotesService } from '~modules/notes/services/notes.service';
import { GetUser } from '~modules/users/decorators/get-user.decorator';
import { CreateNoteDto } from '~modules/notes/dtos/create-note.dto';
import { UpdateNoteDto } from '~modules/notes/dtos/update-note.dto';
import { GetAllQueryDto } from '~modules/notes/dtos/get-all-query.dto';
import { NoteIdParamDto } from '~modules/notes/dtos/note-id-param.dto';
import { JwtTokenGuard } from '~modules/jwt-token/guards/jwt-token.guard';
import { UserDoc } from '~modules/users/schemas/user.schema';

@Controller('notes')
@UseGuards(JwtTokenGuard)
export class NotesController {
  constructor(
    private readonly _notesService: NotesService
  ) {}

  @Get()
  async getMany(
    @GetUser() user: UserDoc,
    @Query() { offset, limit }: GetAllQueryDto
  ) {
    const notes = await this._notesService.getMany(user, { offset, limit });
    return { offset, limit, count: notes.length, notes };
  }

  @Post()
  @HttpCode(HttpStatus.OK)
  async create(
    @GetUser() user: UserDoc,
    @Body() createNoteDto: CreateNoteDto
  ) {
    await this._notesService.create(user, createNoteDto);
    return { message: 'Success' };
  }

  @Get(':id')
  async getOne(
    @GetUser() user: UserDoc,
    @Param() { id }: NoteIdParamDto
  ) {
    const note = await this._notesService.getOne(user, id);
    return { note };
  }

  @Put(':id')
  async updateText(
    @GetUser() user: UserDoc,
    @Param() { id }: NoteIdParamDto,
    @Body() updateNoteDto: UpdateNoteDto
  ) {
    const note = await this._notesService.getOne(user, id);
    await this._notesService.updateText(note, updateNoteDto);
    return { message: 'Success' };
  }

  @Patch(':id')
  async updateCompletedStatus(
    @GetUser() user: UserDoc,
    @Param() { id }: NoteIdParamDto
  ) {
    const note = await this._notesService.getOne(user, id);
    await this._notesService.updateCompletedStatus(note);
    return { message: 'Success' };
  }

  @Delete(':id')
  async delete(
    @GetUser() user: UserDoc,
    @Param() { id }: NoteIdParamDto
  ) {
    const note = await this._notesService.getOne(user, id);
    await this._notesService.deleteOne(user, note);
    return { message: 'Success' };
  }
}
