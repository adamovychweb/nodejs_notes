import { BadRequestException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { NoteDoc, NoteSchemaName } from '~modules/notes/schemas/note.schema';
import { UpdateNoteDto } from '~modules/notes/dtos/update-note.dto';
import { CreateNoteDto } from '~modules/notes/dtos/create-note.dto';
import { UserDoc } from '~modules/users/schemas/user.schema';

@Injectable()
export class NotesService {
  constructor(
    @InjectModel(NoteSchemaName)
    private readonly _noteModel: Model<NoteDoc>
  ) {}

  async getMany(user: UserDoc, { offset, limit }: {offset: number, limit: number}) {
    return this._noteModel.find({ userId: user._id })
      .skip(offset)
      .limit(limit);
  }

  async create(user: UserDoc, createNoteDto: CreateNoteDto) {
    const note = await this._noteModel.create({ userId: user._id, ...createNoteDto });
    user.notes.push(note);
    await user.save();
    return note;
  }

  async getOne(user: UserDoc, noteId: string) {
    const notes = await user.populate('notes')
      .then((user) => user.notes as NoteDoc[]);
    const note = notes.find((note) => note.id === noteId);
    if (!note) {
      throw new BadRequestException('Nothing found');
    }
    return note;
  }

  async updateText(note: NoteDoc, updateNoteDto: UpdateNoteDto) {
    await this._noteModel.updateOne({ _id: note._id }, updateNoteDto);
  }

  async updateCompletedStatus(note: NoteDoc) {
    note.completed = !note.completed;
    await note.save();
  }

  async deleteOne(user: UserDoc, note: NoteDoc) {
    await this._noteModel.deleteOne({ _id: note._id });
    user.notes = user.notes.filter((userNote: NoteDoc) => userNote._id !== note._id);
    await user.save();
  }

  async deleteMany(user: UserDoc) {
    await this._noteModel.deleteMany({ userId: user._id });
  }
}
