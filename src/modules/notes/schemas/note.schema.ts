import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Types } from 'mongoose';

import { User } from '~modules/users/schemas/user.schema';

@Schema({ versionKey: false })
export class Note {
  @Prop({ type: Types.ObjectId, ref: 'User', immutable: true })
  userId: User;

  @Prop({ default: false })
  completed: boolean;

  @Prop({ required: true })
  text: string;

  @Prop({ default: () => Date.now(), immutable: true })
  createdDate: Date;
}

export type NoteMongo = Note & {_id: ObjectId}
export type NoteDoc = Note & Document
export const NoteSchema = SchemaFactory.createForClass(Note);
export const NoteSchemaName = Note.name;
