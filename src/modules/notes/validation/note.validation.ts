import { IsNotEmpty, IsString } from 'class-validator';

import { NoteMongo } from '~modules/notes/schemas/note.schema';

export class NoteValidation implements Pick<NoteMongo, 'text'> {
  @IsNotEmpty()
  @IsString()
  text: string;
}
