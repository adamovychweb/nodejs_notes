import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { NotesController } from '~modules/notes/controllers/notes.controller';
import { NotesService } from '~modules/notes/services/notes.service';
import { UsersModule } from '~modules/users/users.module';
import { JwtTokenModule } from '~modules/jwt-token/jwt-token.module';
import { NoteSchema, NoteSchemaName } from '~modules/notes/schemas/note.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: NoteSchemaName, schema: NoteSchema }
    ]),
    forwardRef(() => UsersModule),
    JwtTokenModule
  ],
  controllers: [NotesController],
  providers: [NotesService],
  exports: [NotesService]
})
export class NotesModule {}
