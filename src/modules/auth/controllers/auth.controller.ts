import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';

import { RegisterUserDto } from '~modules/auth/dtos/register-user.dto';
import { AuthService } from '~modules/auth/services/auth.service';
import { LoginUserDto } from '~modules/auth/dtos/login-user.dto';
import { JwtTokenService } from '~modules/jwt-token/services/jwt-token.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly _authService: AuthService,
    private readonly _jwtTokenService: JwtTokenService
  ) {}

  @Post('register')
  @HttpCode(HttpStatus.OK)
  async register(
    @Body() registerUserDto: RegisterUserDto
  ) {
    await this._authService.register(registerUserDto);
    return { message: 'Success' };
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  public async login(
    @Body() loginUserDto: LoginUserDto
  ) {
    const { token } = await this._authService.login(loginUserDto);
    return { 'message': 'Success', 'jwt_token': token };
  }
}
