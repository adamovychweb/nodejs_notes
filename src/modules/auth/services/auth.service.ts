import { Injectable } from '@nestjs/common';

import { UsersService } from '~modules/users/services/users.service';
import { RegisterUserDto } from '~modules/auth/dtos/register-user.dto';
import { JwtTokenService } from '~modules/jwt-token/services/jwt-token.service';
import { LoginUserDto } from '~modules/auth/dtos/login-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly _usersService: UsersService,
    private readonly _jwtTokenService: JwtTokenService
  ) {}

  public async register({ username, password }: RegisterUserDto) {
    await this._usersService.checkUsernameUsed(username);
    await this._usersService.create({ username, password });
  }

  public async login({ username, password }: LoginUserDto) {
    const user = await this._usersService.findByUsername(username);
    await this._usersService.verifyPassword(user, password);
    const token = await this._jwtTokenService.generateToken({ id: user.id });
    return { token };
  }
}
