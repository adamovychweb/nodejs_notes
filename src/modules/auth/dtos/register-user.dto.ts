import { PickType } from '@nestjs/swagger';

import { UserValidation } from '~modules/users/validation/user.validation';

export class RegisterUserDto extends PickType(
  UserValidation, ['username', 'password'] as const
) {}
