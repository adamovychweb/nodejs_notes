import { PickType } from '@nestjs/swagger';

import { UserValidation } from '~modules/users/validation/user.validation';

export class LoginUserDto extends PickType(
  UserValidation, ['username', 'password'] as const
) {}
