import { Module } from '@nestjs/common';

import { AuthController } from '~modules/auth/controllers/auth.controller';
import { AuthService } from '~modules/auth/services/auth.service';
import { UsersModule } from '~modules/users/users.module';
import { JwtTokenModule } from '~modules/jwt-token/jwt-token.module';

@Module({
  imports: [UsersModule, JwtTokenModule],
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule {}
