import { IsNotEmpty, IsString } from 'class-validator';
import { UserMongo } from '~modules/users/schemas/user.schema';

export type UserValidationType = Pick<UserMongo, 'username' | 'password'>

export class UserValidation implements UserValidationType {
  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  password: string;
}
