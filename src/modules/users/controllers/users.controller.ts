import { Body, Controller, Delete, Get, Patch, UseGuards } from '@nestjs/common';
import { UsersService } from '~modules/users/services/users.service';
import { GetUser } from '~modules/users/decorators/get-user.decorator';
import { ReadableUserSerializer } from '~modules/users/serializers/readable-user.serializer';
import { JwtTokenGuard } from '~modules/jwt-token/guards/jwt-token.guard';
import { ChangePasswordDto } from '~modules/users/dtos/change-password.dto';
import { UserDoc } from '~modules/users/schemas/user.schema';

@Controller('users/me')
@UseGuards(JwtTokenGuard)
export class UsersController {
  constructor(
    private readonly _usersService: UsersService
  ) {}

  @Get()
  async getInfo(
    @GetUser() user: UserDoc
  ) {
    return { user: new ReadableUserSerializer(user) };
  }

  @Delete()
  async delete(
    @GetUser() user: UserDoc
  ) {
    await this._usersService.delete(user);
    return { message: 'Success' };
  }

  @Patch()
  async changePassword(
    @GetUser() user: UserDoc,
    @Body() changePasswordDto: ChangePasswordDto
  ) {
    await this._usersService.changePassword(user, changePasswordDto);
    return { message: 'Success' };
  }
}
