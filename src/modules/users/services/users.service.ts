import { BadRequestException, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { UserDoc, UserSchemaName } from '~modules/users/schemas/user.schema';
import { ChangePasswordDto } from '~modules/users/dtos/change-password.dto';
import { RegisterUserDto } from '~modules/auth/dtos/register-user.dto';
import { NotesService } from '~modules/notes/services/notes.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(UserSchemaName)
    private readonly _userModel: Model<UserDoc>,
    private readonly _notesService: NotesService
  ) {}

  async create({ username, password }: RegisterUserDto): Promise<UserDoc> {
    const hashedPassword = await bcrypt.hash(password, 10);
    return this._userModel.create({ username, password: hashedPassword });
  }

  async findById(userId: string): Promise<UserDoc> {
    return this._userModel.findById(userId);
  }

  async findByUsername(username: string): Promise<UserDoc> {
    const user = await this._userModel.findOne({ username });
    if (!user) {
      throw new BadRequestException(`User with this username doesn't exists`);
    }
    return user;
  }

  async checkUsernameUsed(username: string): Promise<void> {
    const user = await this._userModel.exists({ username });
    if (user) {
      throw new BadRequestException('This username is already used');
    }
  }

  async delete(user: UserDoc): Promise<void> {
    await this._userModel.deleteOne({ _id: user._id });
    await this._notesService.deleteMany(user);
  }

  async changePassword(
    user: UserDoc,
    { oldPassword, newPassword }: ChangePasswordDto
  ): Promise<void> {
    await this.verifyPassword(user, oldPassword);
    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();
  }

  async verifyPassword(user: UserDoc, password): Promise<void> {
    const isCorrect = await bcrypt.compare(password, user.password);
    if (!isCorrect) {
      throw new BadRequestException('Wrong password');
    }
  }
}
