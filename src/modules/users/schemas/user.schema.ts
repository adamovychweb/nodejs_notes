import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Types } from 'mongoose';

import { Note } from '~modules/notes/schemas/note.schema';

@Schema({ versionKey: false })
export class User {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop({
    default: () => Date.now(),
    immutable: true
  })
  createdDate: Date;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Note' }] })
  notes: Note[];
}

export type UserMongo = User & {_id: ObjectId}
export type UserDoc = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);
export const UserSchemaName = User.name;
