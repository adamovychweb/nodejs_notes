import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UsersService } from '~modules/users/services/users.service';
import { UserSchema, UserSchemaName } from '~modules/users/schemas/user.schema';
import { UsersController } from '~modules/users/controllers/users.controller';
import { JwtTokenModule } from '~modules/jwt-token/jwt-token.module';
import { NotesModule } from '~modules/notes/notes.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: UserSchemaName, schema: UserSchema }
    ]),
    forwardRef(() => JwtTokenModule),
    forwardRef(() => NotesModule)
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
