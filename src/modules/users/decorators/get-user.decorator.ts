import { createParamDecorator, ExecutionContext, UnauthorizedException } from '@nestjs/common';

export const GetUser = createParamDecorator(
  (data, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    if (request.customAppData?.user) {
      return request.customAppData.user;
    }
    throw new UnauthorizedException();
  }
);
