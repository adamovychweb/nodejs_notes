import { UserDoc } from '~modules/users/schemas/user.schema';

export class ReadableUserSerializer {
  readonly _id: string;
  readonly username: string;
  readonly createdDate: Date;

  constructor(user: UserDoc) {
    this._id = user._id.toString();
    this.username = user.username;
    this.createdDate = user.createdDate;
  }
}
