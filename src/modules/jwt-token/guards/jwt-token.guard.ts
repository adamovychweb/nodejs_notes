import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { UsersService } from '~modules/users/services/users.service';
import { JwtTokenService } from '~modules/jwt-token/services/jwt-token.service';

@Injectable()
export class JwtTokenGuard implements CanActivate {
  constructor(
    private readonly _jwtTokenService: JwtTokenService,
    private readonly _usersService: UsersService
  ) {}

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();
    const accessToken = this._jwtTokenService.getToken(req);
    const user = await this._jwtTokenService.decodeToken(accessToken);
    req.customAppData = { user };
    return true;
  }
}
