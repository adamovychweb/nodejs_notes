import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { UsersModule } from '~modules/users/users.module';
import { JwtTokenService } from '~modules/jwt-token/services/jwt-token.service';
import { JwtTokenGuard } from '~modules/jwt-token/guards/jwt-token.guard';

@Module({
  imports: [
    JwtModule.register({}),
    forwardRef(() => UsersModule)
  ],
  providers: [JwtTokenService, JwtTokenGuard],
  exports: [JwtTokenService, JwtTokenGuard]
})
export class JwtTokenModule {}
