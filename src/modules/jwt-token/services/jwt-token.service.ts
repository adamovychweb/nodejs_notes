import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import * as config from 'config';

import { UsersService } from '~modules/users/services/users.service';

@Injectable()
export class JwtTokenService {
  constructor(
    private readonly _jwtService: JwtService,
    private readonly _usersService: UsersService
  ) {}

  async generateToken(payload: {id: string}) {
    return this._jwtService.sign(payload, {
      secret: config.get<string>('jwt.secret'),
      expiresIn: config.get<number>('jwt.expires_in')
    });
  }

  async decodeToken(token: string) {
    try {
      const { id } = this._jwtService.verify(token, {
        secret: config.get<string>('jwt.secret')
      });
      return this._usersService.findById(id);
    } catch {
      throw new UnauthorizedException('Token is not valid');
    }
  }

  getToken(req: Request) {
    const authorization = req.headers.authorization;
    if (!authorization) {
      throw new UnauthorizedException('No Authorization token');
    }
    return authorization.split(' ')[1];
  }
}

