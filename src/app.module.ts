import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import * as config from 'config';

import { UsersModule } from '~modules/users/users.module';
import { AuthModule } from '~modules/auth/auth.module';
import { NotesModule } from '~modules/notes/notes.module';
import { JwtTokenModule } from '~modules/jwt-token/jwt-token.module';

@Module({
  imports: [
    MongooseModule.forRoot(config.get<string>('database.url')),
    AuthModule,
    JwtTokenModule,
    NotesModule,
    UsersModule
  ]
})
export class AppModule {}
